#! /usr/bin/python3.9

import hvac
import json
import os

## HASHICORP VAULT CONFIGURATION
VAULT_URL = "https://vault-cluster-public-vault-5994a63e.f63de60e.z1.hashicorp.cloud:8200"
VAULT_TOKEN = "hvs.CAESIPyROJGHQgU7ka_Z-2_FbtRUbBnFuhrAU4MtAwIdZwTIGigKImh2cy5Telh1UTZPNnhWZ0JNa1NOTnJ3Z0VWdDMualNmbmgQus8E"
MOUNT_PATH = 'kv'

## ANSIBLE CONFIGURATION

LINUX_CONNECT = 'ssh'
WIN_CONNECT = 'winrm'
WINRM_PORT = 5986 # Use 5986 for HTTPS to connect. 5985 for HTTP can be used as well.
WINRM_TRANSPORT = 'basic'
WINRM_CERT_VALIDATION = 'ignore'

## GENERAL CONFIGURATION

PRIVATE_KEY_DIR = 'keys' # Folder to store private keys

def savePrivateKeyToFile(sshPrivateKey, filename):
    privateKeyPath = os.path.join(PRIVATE_KEY_DIR, filename)
    if os.path.isdir(PRIVATE_KEY_DIR) == False:
        os.mkdir(PRIVATE_KEY_DIR)
    else:
        # print("Directory already exists: " + PRIVATE_KEY_DIR)
        pass
    with open(privateKeyPath, 'w') as f:
        f.write(sshPrivateKey)
    if os.path.exists(privateKeyPath):
        # print("Successfully create private key file at: " + privateKeyPath)
        os.chmod(privateKeyPath, 0o600)
        # print("Successfully change permission of private key file to 600")

def getInventoryJSON(client):

    # Initialize
    returnedJSON = {}
    returnedJSON["_meta"] = {}
    returnedJSON["_meta"]["hostvars"] = {}

    # Retrieve secret

    secret_response = client.secrets.kv.v2.list_secrets(
        mount_point = MOUNT_PATH,
        path='mylab01'
    )

    secret_list = secret_response['data']['keys']

    index_win = 1
    index_linux = 1
    win_groupName = "win"
    linux_groupName = "linux"
    for secret in secret_list:
        secret_items_response = client.secrets.kv.v2.read_secret_version(
            raise_on_deleted_version = True,
            mount_point = MOUNT_PATH,
            path= "mylab01" + '/' + secret
        )
        secret_items = secret_items_response['data']['data']

        if secret.startswith('win'):
            win_hostname = "win" + str(index_win)
            if win_groupName not in returnedJSON:
                returnedJSON[win_groupName] = {}
                returnedJSON[win_groupName]["hosts"] = []
            returnedJSON[win_groupName]["hosts"].append(win_hostname)
            returnedJSON["_meta"]["hostvars"][win_hostname] = {}
            returnedJSON["_meta"]["hostvars"][win_hostname]["ansible_host"] = secret_items["host"]
            returnedJSON["_meta"]["hostvars"] [win_hostname]["ansible_user"] = secret_items["username"]
            returnedJSON["_meta"]["hostvars"] [win_hostname]["ansible_password"] = secret_items["password"]
            returnedJSON["_meta"]["hostvars"] [win_hostname]["ansible_port"] = WINRM_PORT
            returnedJSON["_meta"]["hostvars"] [win_hostname]["ansible_connection"] = WIN_CONNECT
            returnedJSON["_meta"]["hostvars"] [win_hostname]["ansible_winrm_server_cert_validation"] = WINRM_CERT_VALIDATION
            returnedJSON["_meta"]["hostvars"] [win_hostname]["ansible_winrm_transport"] = WINRM_TRANSPORT
            index_win += 1

        elif secret.startswith('ubuntu') or secret.startswith('centos') or secret.startswith('rhel'):
            linux_hostname = secret_items["username"] + str(index_linux)
            savePrivateKeyToFile(secret_items["private_ssh_key"],  linux_hostname + ".pem")

            if linux_groupName not in returnedJSON:
                returnedJSON[linux_groupName] = {}
                returnedJSON[linux_groupName]["hosts"] = []
            returnedJSON[linux_groupName]["hosts"].append(linux_hostname)
            returnedJSON["_meta"]["hostvars"][linux_hostname] = {}
            returnedJSON["_meta"]["hostvars"][linux_hostname]["ansible_host"] = secret_items["host"]
            returnedJSON["_meta"]["hostvars"][linux_hostname]["ansible_user"] = secret_items["username"]
            returnedJSON["_meta"]["hostvars"][linux_hostname]["ansible_ssh_private_key_file"] = os.path.join(PRIVATE_KEY_DIR, linux_hostname + ".pem")
            returnedJSON["_meta"]["hostvars"][linux_hostname]["ansible_connection"] = LINUX_CONNECT
            index_linux += 1
    return returnedJSON

if __name__ == '__main__':

    # Authentication

    client = hvac.Client(
        url=VAULT_URL,
        token=VAULT_TOKEN,
        namespace="admin/"
    )

    inventoryJSON = getInventoryJSON(client)
    print(json.dumps(inventoryJSON))